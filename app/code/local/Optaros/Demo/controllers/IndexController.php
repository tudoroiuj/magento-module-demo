<?php

class Optaros_Demo_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction()
    {

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Formular de adaugare"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("formular de adaugare", array(
            "label" => $this->__("Formular de adaugare"),
            "title" => $this->__("Formular de adaugare")
        ));

        $this->renderLayout();

    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();

        if ($post) {

            $data = array(
                'nume' => $post['nume'],
                'prenume' => $post['prenume'],
                'date_of_birth' => $post['date_of_birth'],
                'cod_discount' => $post['cod_discount'],
                'create_at' => NOW(),
                'updated_at' => NOW()
            );
            $model = Mage::getModel('demo/demomodel')->setData($data);
            try {
                $insertId = $model->save()->getId();
                $message = "Datele au fost inserate cu succes. ID - ul inregistrarii este : " . $insertId;
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        } else {
            $message = "nu avem ce insera";
        }
        Mage::register('message', $message);

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Formular de adaugare"));

        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("formular de adaugare", array(
            "label" => $this->__("Formular de adaugare"),
            "title" => $this->__("Formular de adaugare"),
            "link" => Mage::getUrl("demo")
        ));

        $breadcrumbs->addCrumb("va multumim", array(
            "label" => $this->__("Va multumim"),
            "title" => $this->__("Va multumim")
        ));

        $this->renderLayout();
    }

    public function testAction()
    {
        echo "test action";
    }
}