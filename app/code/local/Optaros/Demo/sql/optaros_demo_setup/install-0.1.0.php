<?php
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS `{$this->getTable('demo_table')}`;
CREATE TABLE `{$this->getTable('demo_table')}` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `nume` varchar(50) NULL default '',
  `prenume` varchar(50) NULL default '',
  `date_of_birth` DATE,
  `cod_discount` varchar(50) NULL default '',
  
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();