<?php

class Optaros_Demo_Model_Resource_Demomodel extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('demo/demomodel', 'custommodel_id');
    }
}