<?php

Class Optaros_Demo_Model_Resource_Demomodel_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('demo/demomodel');
    }
}